-- List created roles
SELECT usename AS role_name,
CASE
   WHEN usesuper AND usecreatedb THEN
       CAST('superuser, create database' AS pg_catalog.text)
   WHEN usesuper THEN
       CAST('superuser' AS pg_catalog.text)
   WHEN usecreatedb THEN
       CAST('create database' AS pg_catalog.text)
   ELSE
       CAST('' AS pg_catalog.text)
   END role_attributes
FROM pg_catalog.pg_user
ORDER BY role_name ASC;

-- list created tables
SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname NOT IN ('pg_catalog', 'information_schema');

-- list user permissions on tables
SELECT grantee, table_schema, table_name, privilege_type, is_grantable, with_hierarchy
FROM information_schema.role_table_grants
WHERE table_schema NOT IN ('pg_catalog', 'information_schema');
